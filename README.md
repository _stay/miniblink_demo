# miniblink_demo

## 编译要求

1. 至少vs2019
	- 目前是vs2022的工程，但是使用的特性目前vs2019上都有，最好是用vs2022，后面等更新jthread之后就会用了
1. node.js
	- 不是vs负载里的那个，是独立安装的，第一次编译vue3Example的时候要手动点npm的节点右键安装包
1. vcpkg
	- `vcpkg integrate install`了才算安装好了，使用manifest模式，不用先把第三方库装好
1. .vsconfig
	- 剩余的`.vsconfig`会处理，打开工程之后安装对应的负载应该就好了

## 工程说明

- main是控制台程序，使用`POPUP`的模式，也可以改成`TRANSPARENT`的模式，静态的`main.html`就是用来测这个，用`-webkit-app-region: drag`来实现拖动的
- MFCApplication1是MFC程序，基本上与`contralpoint.html`交互
- vue3Example是vue3的工程，用来写前端
	- calculator，给main用的，调main的exprtk计算
	- city，自身就可以加载
	- radios，给main用的，用来测调用c++函数的
	- table，给main用的，用来测c++初始数据传递给前端的
- main.html，给main用的，用来测`TRANSPARENT`的
