#pragma once
#include <filesystem>
#include "../wke.h"

#ifdef _WIN64
const inline std::filesystem::path kNodeDll{ "miniblink_4975_x64.dll" };
#else
const inline std::filesystem::path kNodeDll{ "miniblink_4975_x32.dll" };
#endif

[[nodiscard]]
inline auto load_mb() -> std::filesystem::path
{
	auto pathRoot = std::filesystem::current_path();
	auto pathDll = pathRoot / kNodeDll;
	if (!std::filesystem::exists(pathDll))
	{
		pathRoot /= "..";
		pathRoot = pathRoot.lexically_normal();
		pathDll = pathRoot / kNodeDll;
		if (!std::filesystem::exists(pathDll))
			return {};
	}

	auto szPathDll = pathDll.wstring();
	wkeSetWkeDllPath(szPathDll.c_str());
	if (!wkeInitialize())
	{
		return {};
	}
	return pathDll;
}
